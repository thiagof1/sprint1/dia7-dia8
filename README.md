# Dia 7 Dia8

Nos dia 7 e 8, foi proposto fazer os cursos AWS, os cursos são :

[Job Roles in the Cloud (Portuguese)](https://explore.skillbuilder.aws/learn/course/4944/job-roles-in-the-cloud-portuguese;lp=1543)

[AWS Cloud Practitioner Essentials](https://explore.skillbuilder.aws/learn/course/8287/aws-cloud-practitioner-essentials-portuguese;lp=1543)

[Getting Started with Cloud Acquisition (Portuguese](https://explore.skillbuilder.aws/learn/course/1396/getting-started-with-cloud-acquisition-portuguese;lp=1543));

[AWS Billing and Cost Management](https://explore.skillbuilder.aws/learn/course/1824/aws-billing-and-cost-management-portuguese;lp=1543);

[AWS Well-Architected (Portuguese)](https://explore.skillbuilder.aws/learn/course/684/aws-well-architected-portuguese;lp=1543);

[Getting Started with the AWS Cloud Essentials (Portuguese)](https://explore.skillbuilder.aws/learn/course/1808/aws-foundations-getting-started-with-the-aws-cloud-essentials-portuguese;lp=1543)

[Fundamentos da AWS: Protegendo sua nuvem](https://explore.skillbuilder.aws/learn/course/1830/fundamentos-da-aws-protegendo-sua-nuvem-portugues-aws-foundations-securing-your-aws-cloud-portuguese;lp=1543)

**No Job Roles in the Cloud (Portuguese):** os principais objetivos do curso são, identificar as reponsabilidade em um ambiente de TI clássico e em um ambiente de nuvem, descrever a infraestrutura como código e seus benefícios, e determinar os times de desenvolvimento em ti.

Em resumo existem muitos serviços de nuvem. Alguns serviços podem não estar ligados a uma função  especifica. A infraestrutura como código é uma pratica em que a  infraestrutura é provisionada e gerenciada  usando técnicas de desenvolvimento de código e de software, como o versionamento e a integração e distribuição continuas .

**AWS Cloud Practitioner Essentials**: nesse curso foi visto  como é desenvolvemento da nuvem AWS e foi aprendido sobre conceitos da nuvem AWS, serviços AWS, segurança, arquitetura, definição de preços e suporte.

O curso é um treinamento introdutório oferecido pela Amazon Web Services (AWS) para fornecer uma visão geral dos serviços, arquitetura, segurança e preços da AWS. É voltado para iniciantes na AWS e ajuda a preparar os profissionais para o exame de certificação AWS Certified Cloud Practitioner.

**Getting Started with Cloud Acquisition**: Neste curso eu tive conhecimentos sobre a compra Nuvem AWS. O curso inclui uma análise aprofundada das melhores práticas de aquisição de nuvem.

**AWS Well-Architected**: aborda o framework de arquitetura bem-arquitetada da AWS, que consiste em cinco pilares fundamentais: excelência operacional, segurança, confiabilidade, eficiência de desempenho e otimização de custos. O objetivo do curso é fornecer orientações e práticas recomendadas para projetar, construir e operar cargas de trabalho eficientes e seguras na nuvem da AWS. Os principais pontos do curso incluem a importância da automação, monitoramento e recuperação de falhas para alcançar a excelência operacional.
